package com.ssaurel.lovewallpaper.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("/s/gd5lq465sv37lif/data.json")
    Call<List<RetroPhoto>> getAllPhotos();
}
