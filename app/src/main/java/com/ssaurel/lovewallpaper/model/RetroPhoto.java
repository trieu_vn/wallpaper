package com.ssaurel.lovewallpaper.model;

import com.google.gson.annotations.SerializedName;

public class RetroPhoto {
    @SerializedName("cat_id")
    private String cat_id;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;
    @SerializedName("thumb")
    private String thumb;

    public RetroPhoto(String cat_id, Integer id, String title, String url, String thumb) {
        this.cat_id = cat_id;
        this.title = title;
        this.url = url;
        this.thumb = thumb;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
