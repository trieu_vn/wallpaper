package com.ssaurel.lovewallpaper.acticity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.ssaurel.lovewallpaper.R;
import com.ssaurel.lovewallpaper.fragment.BottomSheetFragment;
import com.ssaurel.lovewallpaper.ultils.SetWallpaper;

import java.io.IOException;

public class ItemActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView imageView;
    private Context context;
    private Button bottomSheetWallpaper, buttonDownload;
    private ProgressBar progressBar;
    private String image_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Set Wallpaper");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        image_url = getIntentImage();
    }

    private void init() {
        setContentView(R.layout.activity_item);
        imageView = findViewById(R.id.itemView);
        bottomSheetWallpaper = findViewById(R.id.bottomSheetWallpaper);
        buttonDownload = findViewById(R.id.buttonDownload);
        progressBar = findViewById(R.id.loadingImage);
        bottomSheetWallpaper.setOnClickListener(this);
    }

    public String getIntentImage() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            image_url = bundle.getString("url_item", "");
        }
        Picasso.with(getBaseContext()).load(image_url).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
                buttonDownload.setVisibility(View.VISIBLE);
                bottomSheetWallpaper.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {
                buttonDownload.setVisibility(View.GONE);
                bottomSheetWallpaper.setVisibility(View.GONE);
            }
        });
        return image_url;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bottomSheetWallpaper:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                    bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                    Bundle bundle = new Bundle();
                    bundle.putString("image_url", image_url);
                    bottomSheetFragment.setArguments(bundle);
                } else {
                    SetWallpaper.getInstance(context).setWallPaper(image_url, "setHomeScreen");
                }
                break;
            case android.R.id.home:
                this.finish();
                break;
        }
    }
}