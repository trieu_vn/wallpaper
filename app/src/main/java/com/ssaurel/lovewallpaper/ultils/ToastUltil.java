package com.ssaurel.lovewallpaper.ultils;

import android.content.Context;
import android.widget.Toast;

public class ToastUltil {
    private static ToastUltil instance;
    private Toast toast;

    public ToastUltil(Context context) {
        toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
    }

    public static ToastUltil getInstance(Context context) {
        if (instance == null) {
            instance = new ToastUltil(context);
        }
        return instance;
    }
    public void showToast(String string){
        toast.setText(string);
        toast.show();
    }
}
