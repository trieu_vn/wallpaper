package com.ssaurel.lovewallpaper.ultils;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;

import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.net.URL;

public class SetWallpaper {
    private static SetWallpaper instance;
    private Context context;

    public SetWallpaper(Context context) {
        this.context = context;
    }

    public static SetWallpaper getInstance(Context context) {
        if (instance == null) {
            instance = new SetWallpaper(context);
        }
        return instance;
    }

    public void setWallPaper(String url_image, String typeSet) {
        WallpaperManager myWallpaperManager = WallpaperManager.getInstance(context);
        Picasso.with(context).load(url_image).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                if (typeSet.equals("setHomeScreen")) {
                    setHomeWallpaper(myWallpaperManager, bitmap);
                    ToastUltil.getInstance(context).showToast("Set Home Screen Wallpaper Successfully");
                } else if (typeSet.equals("setLockScreen")) {
                    setLockWallpaper(myWallpaperManager, bitmap);
                    ToastUltil.getInstance(context).showToast("Set Home Screen Wallpaper Successfully");
                } else if (typeSet.equals("setAllScreen")) {
                    setHomeWallpaper(myWallpaperManager, bitmap);
                    setLockWallpaper(myWallpaperManager, bitmap);
                    ToastUltil.getInstance(context).showToast("Set Wallpaper Successfully");
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });


    }

    private void setHomeWallpaper(WallpaperManager wallpaperManager, Bitmap bitmap) {
        try {
            wallpaperManager.setBitmap(bitmap);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void setLockWallpaper(WallpaperManager wallpaperManager, Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                wallpaperManager.setBitmap(bitmap, null, true, WallpaperManager.FLAG_LOCK);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            Toast.makeText(context, "Lock screen walpaper not supported", Toast.LENGTH_SHORT).show();
        }
    }
}
