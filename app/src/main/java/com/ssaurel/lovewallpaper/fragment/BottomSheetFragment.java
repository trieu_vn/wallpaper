package com.ssaurel.lovewallpaper.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ssaurel.lovewallpaper.R;
import com.ssaurel.lovewallpaper.ultils.SetWallpaper;
import com.ssaurel.lovewallpaper.ultils.ToastUltil;

import java.io.IOException;

public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private TextView setHomeScreen;
    private TextView setLockScreen;
    private TextView setAllScreen;
    private String url_image;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet, container, false);
        url_image = this.getArguments().getString("image_url");
        init(view);
        return view;
    }

    private void init(View view) {
        setLockScreen = view.findViewById(R.id.setLockScreen);
        setHomeScreen = view.findViewById(R.id.setHomeScreen);
        setAllScreen = view.findViewById(R.id.setAllScreen);
        setLockScreen.setOnClickListener(this);
        setHomeScreen.setOnClickListener(this);
        setAllScreen.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setHomeScreen:
                SetWallpaper.getInstance(getContext()).setWallPaper(url_image, "setHomeScreen");
                ToastUltil.getInstance(getContext()).showToast("Set Home Wallpaper Successfully");
                break;
            case R.id.setLockScreen:

                SetWallpaper.getInstance(getContext()).setWallPaper(url_image, "setLockScreen");
                ToastUltil.getInstance(getContext()).showToast("Set Lock Wallpaper Successfully");
                break;
            case R.id.setAllScreen:

                SetWallpaper.getInstance(getContext()).setWallPaper(url_image, "setAllScreen");
                ToastUltil.getInstance(getContext()).showToast("Set All Wallpaper Successfully");
                break;
        }
    }
}
